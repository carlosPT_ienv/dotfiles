" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
endif

autocmd VimEnter * source $MYVIMRC

call plug#begin('~/.config/nvim/autoload/plugged')

    " Themes and colorSchemes
    Plug 'jacoborus/tender.vim'
    Plug 'gruvbox-community/gruvbox'
    Plug 'sts10/vim-pink-moon'
    Plug 'sonph/onehalf', {'rtp': 'vim/'}
    Plug 'mhartington/oceanic-next'
    Plug 'dracula/vim', { 'as': 'dracula' }
    Plug 'altercation/vim-colors-solarized'
    Plug 'jnurmine/Zenburn'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'ap/vim-css-color'
    Plug 'vim-scripts/guicolorscheme.vim'
    Plug 'bluz71/vim-nightfly-guicolors'

    " Git integration
    Plug 'tpope/vim-fugitive'
    Plug 'airblade/vim-gitgutter'
    Plug 'mbbill/undotree'
    Plug 'justinmk/vim-sneak'
    Plug 'APZelos/blamer.nvim'
    Plug 'junegunn/gv.vim'   

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    Plug 'jiangmiao/auto-pairs'
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-commentary'
    Plug 'nathanaelkane/vim-indent-guides'
    Plug 'Yggdroot/indentLine'
    Plug 'ryanoasis/vim-devicons'

    " JS and TS syntax
    Plug 'pangloss/vim-javascript'
    Plug 'leafgarland/typescript-vim'
    
    " Auto completion Plugins
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/nvim-cmp'
    Plug 'hrsh7th/cmp-buffer'
    Plug 'hrsh7th/cmp-path'
    Plug 'hrsh7th/cmp-cmdline'

    " Snippet Plugins
    Plug 'hrsh7th/cmp-vsnip'
    Plug 'hrsh7th/vim-vsnip'

    " AutoFormater Plugin
    Plug 'jose-elias-alvarez/null-ls.nvim' 

    " Rust LSP Client
    Plug 'simrat39/rust-tools.nvim'

    " Markdown 
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

    " Optional dependencies
    Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'

    " Debugging (needs plenary from above as well)
    Plug 'mfussenegger/nvim-dap'
    
    " Sensible Completion
    if has('nvim')
        Plug 'neovim/nvim-lspconfig'
"        Plug 'glepnir/lspsaga.nvim'
        Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
"        Plug 'nvim-lua/completion-nvim'
    else
"        Plug 'Shougo/deoplete.nvim'
"        Plug 'roxma/nvim-yarp'
"        Plug 'roxma/vim-hug-neovim-rpc'
    endif
"    Plug 'Shougo/neosnippet.vim'
"    Plug 'Shougo/neosnippet-snippets'

    " File Managers
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'airblade/vim-rooter'

call plug#end()

