" init autocmd
autocmd!

" true color
if exists("&termguicolors") 
  syntax enable
  set termguicolors
  set winblend=0
  set wildoptions=pum
  set pumblend=5
  set background=dark
  " colorscheme gruvbox
  " colorscheme nightfly
  " Use NeoSolarized
  "let g:neosolarized_termtrans=1
  "runtime ./colors/NeoSolarized.vim
  "runtime ./colors/gruvbox-primegean.vim
  " colorscheme zenburn 
  "colorscheme gruvbox-primegean
  " colorscheme NeoSolarized
endif

" autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

if $VIRTUAL_ENV == " "
    echo $VIRTUAL_ENV
endif

echo $VIRTUAL_ENV
let g:python3_host_prog = "$VIRTUAL_ENV/bin/python"


" Run gofmt on save
autocmd BufWritePre *.go :call LanguageClient#textDocument_formatting_sync()

filetype on
filetype indent on
set encoding=UTF-8

autocmd User TelescopePreviewerLoaded setlocal wrap

" Very useful for yaml files
autocmd FileType yaml set cursorcolumn
autocmd FileType yml set cursorcolumn


" Nice menu when typing `:find *.py`
set wildmode=longest,list,full
set wildmenu
" Ignore files
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=**/coverage/*
set wildignore+=**/node_modules/*
set wildignore+=**/android/*
set wildignore+=**/ios/*
set wildignore+=**/.git/*

set hidden
set cursorline
set number
set nowrap
set colorcolumn=80
set signcolumn=yes
set scrolloff=8
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set listchars
" Give more space for displaying messages.
set cmdheight=1

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=150


set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
" Enable folding
set foldmethod=indent
set foldlevel=99

" Use 256 colors in gui_running mode
" if !has('gui_running')
  " set t_Co=256
" endif

" Hybrid Line numbers
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Set Width Line Limit to Column number 80 and show the excess in red
augroup vimrc_autocmds
  autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#592929
  autocmd BufEnter * match OverLength /\%88v.*/
augroup END


" Native netrw Explorer
nnoremap - :Explore<CR>
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro'
autocmd FileType netrw setl bufhidden=delete


set completeopt-=preview
set completeopt+=noinsert

" Switching Buffers
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>
imap <C-N> <Esc>:bnext<CR>i
imap <C-P> <Esc>:bprev<CR>i



" autocmd FileType python map <buffer> <F8> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
" autocmd FileType python imap <buffer> <F8> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>


" search
set incsearch " incremental search
set ignorecase " search is case insensitive but you can add \C to make it sensitive
set smartcase " will automatically switch to a case-sensitive search if you use any capital letters

