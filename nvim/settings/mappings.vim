" ============================= 
" mappings
" ============================== 

" :map and :noremap are recursive and non-recursive

let mapleader=","
" let mapleader="\<space>"
" nnoremap <silent> <leader> :LeaderMapper "<Space>"<CR>
" vnoremap <silent> <leader> :LeaderMapper "<Space>"<CR>
" inoremap <silent> <leader> :"<Space>"

" quit file
nnoremap <leader>q <esc>:q<cr>
inoremap <leader>q <esc>:q<cr>

" go to normal mode. amazing!!
inoremap kj <esc>l

" how to split windows
" set splitbelow
set splitright


" resize horizontal splits when windows are reduced
au VimResized *:wincmd = 
nnoremap <silent> <leader>+ :resize +1<CR> 
nnoremap <silent> <leader>- :resize -1<CR> 

" resize vertical splits when windows are reduced
au VimResized *:wincmd = 
nnoremap <silent> <leader>++ :vertical resize +5<CR> 
nnoremap <silent> <leader>-- :vertical resize -5<CR> 


" Show border in windows vertical splits
set fillchars+=vert:\┊

" move between windows
noremap <TAB><TAB> <C-w><C-w>

" abbreviations
ab wcm ✅

" do nothing when F1 is pressed
noremap <F1> <nop>
inoremap <F1> <nop>

" key mapping for save file
nnoremap <F2> <esc>:w<CR>
inoremap <F2> <esc>:w<CR>
noremap <F2> <esc>:w<CR>
nnoremap <leader>s <esc>:w<CR>
" inoremap <leader>s <esc>:w<CR>
vnoremap <leader>s <esc>:w<CR>

" tab navigation mappings
nnoremap tn :tabn<CR>
nnoremap tp :tabp<CR>
nnoremap tt :$tabnew<CR>

" Horizontal Split
nnoremap <leader>sh :split<CR>:terminal<CR>
noremap <leader>sp :vsplit<CR>

" move text lines and blocks
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" Git Mapping
nnoremap gs :Git<CR>
" nnoremap gb :Git blame<CR>

" FZF Mapping
map <C-f> :Files<CR>
map <leader>b :Buffers<CR>
nnoremap <leader>l :Lines<CR>
" nnoremap <leader>g :Rg<CR>
nnoremap <leader>t :Tags<CR>
nnoremap <leader>m :Marks<CR>

" Markdown-Preview 
nmap <C-s> <Plug>MarkdownPreview
nmap <M-s> <Plug>MarkdownPreviewStop
nmap <C-p> <Plug>MarkdownPreviewToggle

nnoremap <leader>d :bdelete<CR>

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>fu <cmd>Telescope current_buffer_fuzzy_find<cr>

" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
" imap <C-k>     <Plug>(neosnippet_expand_or_jump)
" smap <C-k>     <Plug>(neosnippet_expand_or_jump)
" xmap <C-k>     <Plug>(neosnippet_expand_target)


" Terminal Mode Mappings
tnoremap <Esc> <C-\><C-n>
