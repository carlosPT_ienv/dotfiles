
source $HOME/.config/nvim/settings/plugins.vim
source $HOME/.config/nvim/settings/config.vim
source $HOME/.config/nvim/settings/mappings.vim
source $HOME/.config/nvim/themes/airline.vim
source $HOME/.config/nvim/themes/gruvbox.vim

" Plugin Configuration
autocmd VimEnter * source $HOME/.config/nvim/plug-config/cmp.rc.vim
autocmd VimEnter * source $HOME/.config/nvim/plug-config/lspconfig.rc.vim
autocmd VimEnter * source $HOME/.config/nvim/plug-config/null_ls.rc.vim
autocmd VimEnter * source $HOME/.config/nvim/plug-config/treesitter.rc.vim
autocmd VimEnter * source $HOME/.config/nvim/plug-config/blamer.rc.vim
" autocmd VimEnter * source $HOME/.config/nvim/plug-config/telescope.rc.vim
" autocmd VimEnter * source $HOME/.config/nvim/plug-config/telescope.rc.vim
source $HOME/.config/nvim/plug-config/telescopetest.vim
source $HOME/.config/nvim/plug-config/markdown-preview.rc.vim
source $HOME/.config/nvim/plug-config/indentLine.vim
source $HOME/.config/nvim/plug-config/nerdfont.vim
source $HOME/.config/nvim/plug-config/fzf.vim
" source $HOME/.config/nvim/plug-config/telescope.rc.vim
" source $HOME/.config/nvim/plug-config/completions.rc.vim
" autocmd VimEnter * source $HOME/.config/nvim/plug-config/lspsaga.rc.vim
" source $HOME/.config/nvim/plug-config/black.vim
" source $HOME/.config/nvim/plug-config/ale.vim
" autocmd BufEnter * source $HOME/.config/nvim/plug-config/rust-tools.rc.lua





