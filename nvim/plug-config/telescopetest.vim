lua << EOF

-- Telescope stuff I need to import for configuration


-- TELESCOPE CONFIG
require('telescope').setup {
  defaults = {
    color_devicons = true,
    prompt_prefix = '🔍 ',
    scroll_strategy = 'cycle',
    sorting_strategy = 'ascending',
    layout_strategy = 'horizontal',
--    file_ignore_patterns = ignore_these,
    layout_config = {
--      prompt_position = 'bottom',
      horizontal = {
--        mirror = true,
        preview_cutoff = 3.2,
        width = 0.9,
--        height = 1,
        preview_width = 0.6,
      },
--      vertical = {
--        mirror = true,
--        preview_cutoff = 0.4,
--      },
--      flex = {
--        flip_columns = 110,
--      },
      height = 0.98,
      width = 0.86,
    },
  },
  extensions = {
    fzf = {
      override_generic_sorter = false,
      override_file_sorter = true,
      case_mode = 'smart_case',
    },
  },
}
-- require fzf extension for fzf sorting algorithm
require('telescope').load_extension 'fzf'




EOF
