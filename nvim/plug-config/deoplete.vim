
call deoplete#custom#option('sources', {
    \ '_': ['LanguageClient'],
    \ 'rust': ['LanguageClient'],
    \ 'auto_complete': v:true,
    \ 'auto_complete_delay': 10,
    \ 'smart_case': v:true,
    \ 'auto_refresh_delay': 10,
    \ 'auto_complete_popup': "auto",
    \},
    \)

let g:deoplete#enable_at_startup=1
"call deoplete#custom#option('sources', { 
"    \ 'rust': ['LanguageClient'],
"    \ }
"    \)

"call deoplete#custom#option('sources', {
"\ '_': ['Ale'],
"\})

