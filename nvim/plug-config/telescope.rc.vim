
lua << EOF
local previewers = require 'telescope.previewers'

require('telescope').setup {
  pickers = default_picker_opts,
  defaults = {
    vimgrep_arguments = {
      'rg',
      '--color=never',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case',
    },
    
    color_devicons = true,
    prompt_prefix = '🔍 ',
    scroll_strategy = 'cycle',
    sorting_strategy = 'ascending',
    layout_strategy = 'horizontal',
--    file_ignore_patterns = ignore_these,
    layout_config = {
      prompt_position = 'bottom',
      horizontal = {
--        mirror = true,
--        width = 0.7,
        preview_cutoff = 100,
--        preview_width = 0.5,
      },
--      vertical = {
--        mirror = true,
--        preview_cutoff = 0.4,
--      },
--      flex = {
--        flip_columns = 110,
--      },
--      height = 0.94,
--      width = 0.86,
    },
  },
  extensions = {
    fzf = {
      override_generic_sorter = false,
      override_file_sorter = true,
      case_mode = 'smart_case',
    },
  },
}

  -- preview_width = 0.5,
  -- other configuration values here

  
require('telescope').load_extension('fzf')


EOF
