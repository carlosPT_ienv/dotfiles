" Vim
let g:indentLine_color_term = 239

let g:indentLine_setColors = 0 

" GVim
let g:indentLine_color_gui = '#535a63'

" none X terminal
let g:indentLine_color_tty_light = 7 " (default: 4)
let g:indentLine_color_dark = 1 " (default: 2)

" Background (Vim, GVim)
" let g:indentLine_bgcolor_term = 202
" let g:indentLine_bgcolor_gui = 'white'

let g:indentLine_char = '┊' 
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

let g:indentLine_concealcursor = 'inc'
let g:indentLine_conceallevel = 1 

let g:indentLine_setConceal = 3 

let g:indentLine_enabled = 1 
