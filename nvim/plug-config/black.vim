autocmd BufWritePre *.py execute ':Black'


let g:black_linelength = 88
" let g:black_virtualenv = g:python3_host_prog 
""


"--------------
" Isort Config
"--------------

let g:vim_isort_map = '<C-i>'

let g:vim_isort_config_overrides = {
  \ 'include_trailing_comma': 1, 'multi_line_output': 3}

