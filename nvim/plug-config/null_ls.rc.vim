lua <<EOF


    local null_ls = require("null-ls")

    -- register any number of sources simultaneously
    local sources = {
        -- Formatting
        null_ls.builtins.formatting.prettier,
        null_ls.builtins.formatting.black,

        null_ls.builtins.formatting.rustfmt.with({
            extra_args = function(params)
                local Path = require("plenary.path")
                local cargo_toml = Path:new(params.root .. "/" .. "Cargo.toml")

                if cargo_toml:exists() and cargo_toml:is_file() then
                    for _, line in ipairs(cargo_toml:readlines()) do
                        local edition = line:match([[^edition%s*=%s*%"(%d+)%"]])
                        if edition then
                            return { "--edition=" .. edition }
                        end
                    end
                end
                -- default edition when we don't find `Cargo.toml` or the `edition` in it.
                return { "--edition=2021" }
            end,
        }),
        -- diagnostics
        null_ls.builtins.diagnostics.flake8,
        -- Hover
        null_ls.builtins.hover.dictionary,
    }

    
    null_ls.setup({
        -- Sources
        sources = sources, 
        on_attach = function(client)
            print(client.name)
            if client.name == "null-ls" then
                client.resolved_capabilities.document_formatting = false 
                vim.cmd ([[
                    augroup LspFormatting 
                        autocmd! * <buffer>
                        autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()
                    augroup END
                ]])
            end
        end,
    })

EOF
