" let g:ale_disable_lsp = 1

let g:ale_sign_column_always = 1

"let g:ale_completion_enabled = 1

"let g:ale_sign_error = '>>'
"let g:ale_sign_warning = '--'
"
"let g:ale_python_executable='/home/ceci/.local/share/virtualenvs/pdf2img-7mKNix3A/bin/python'
"let g:airline#extensions#ale#enabled = 0

"let g:ale_python_black_change_directory=1 
"let g:ale_python_auto_pipenv=1 
let b:ale_python_black_executable=g:python3_host_prog
"let b:ale_python_flake8_executable='pipenv'
"let b:ale_python_pyls_executable='pipenv'
"let g:ale_python_isort_executable='pipenv'
"let g:ale_python_black_auto_pipenv=1 
"let g:ale_python_flake8_auto_pipenv=1
"let g:ale_python_pyls_auto_pipenv=1
"let g:ale_python_isort_auto_pipenv=1


"-----------------------------
" PYTHON
"-----------------------------

". Check Python files with flake8 and pylint.
" let g:ale_linters = {'python': ['flake8']}

"let g:ale_python_pyls_auto_pipenv=1
"let b:ale_linters = ['flake8', 'pylint']
" Fix Python files with autopep8 and yapf.
let g:ale_fixers = {'python': ['black', 'isort']}

" let g:ale_set_quickfix=1


" Disable warnings about trailing whitespace for Python files.
" let b:ale_warn_about_trailing_whitespace = 1

" Fixer options
" RUST
"let g:ale_rust_analyzer_executable = 'rust-analyzer'
" let g:ale_linters = {'rust': ['analyzer']}
let g:ale_fixers = {'rust': ['rustfmt']}

"-----------------------------
" JAVASCRIPT
"-----------------------------


" Static Analysis for TS and JS
let b:ale_linters = ['eslint']

" Fix CSS and JS-TS Files
let g:ale_fixers = {'javascript': ['prettier'], 'css': ['prettier']}
let g:ale_fix_on_save=1

"let g:ale_javascript_prettier_options = '--single-quote --trailing-comma all'

