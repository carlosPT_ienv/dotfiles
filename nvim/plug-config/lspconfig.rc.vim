if !exists('g:lspconfig')
    finish
endif

lua << EOF
local nvim_lsp = require('lspconfig')

local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_set_option(bufnr, ...) end
    
  --mappings
  local opts = { noremap = true, silent = true }

  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap("n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
  buf_set_keymap("n", "<space>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
  buf_set_keymap("n", "<space>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
  buf_set_keymap("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
  buf_set_keymap("n", "<space>e", "<cmd>lua vim.diagnostic.open_float()<CR>", opts) 
end


-- Setup lspconfig.
local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

local servers = { 'rust_analyzer', 'emmet_ls' }

for _, lsp in pairs(servers) do
--    print(lsp)
    require('lspconfig')[lsp].setup {
        on_attach = on_attach,
        capabilities = capabilities,
        flags = {
        -- This will be the default in neovim 0.7+
        debounce_text_changes = 150,
        }
    }
end

require'lspconfig'.pylsp.setup {
    --filetypes = { "typescript", "typescriptreact", "typescript.tsx" }
    on_attach = on_attach,
    capabilities = capabilities,
    root_files = {'pyproject.toml'},
}

require'lspconfig'.tsserver.setup {
    --filetypes = { "typescript", "typescriptreact", "typescript.tsx" }
    on_attach = on_attach,
    filetypes = { "typescript", "typescriptreact", "typescript.tsx" }
}

require'lspconfig'.angularls.setup {
    cmd = { "ngserver", "--stdio", "--tsProbeLocations", "", "--ngProbeLocations", "" },
    filetypes = { "typescript", "html", "typescript.tsx" }
}


require'lspconfig'.tailwindcss.setup{
    cmd = { "tailwindcss-language-server", "--stdio" },
    filetypes = { "aspnetcorerazor", "astro", "astro-markdown", "blade", "django-html", "htmldjango", "edge", "eelixir", "ejs", "erb", "eruby", "gohtml", "haml", "handlebars", "hbs", "html", "html-eex", "heex", "jade", "leaf", "liquid", "markdown", "mdx", "mustache", "njk", "nunjucks", "php", "razor", "slim", "twig", "css", "less", "postcss", "sass", "scss", "stylus", "sugarss", "javascript", "javascriptreact", "reason", "rescript", "typescript", "typescriptreact", "vue", "svelte" },
    init_options = {
      userLanguages = {
        eelixir = "html-eex",
        eruby = "erb"
      }
    },
    on_new_config = function(new_config)
            if not new_config.settings then
                new_config.settings = {}
            end
            if not new_config.settings.editor then
                new_config.settings.editor = {}
            end
            if not new_config.settings.editor.tabSize then
            -- set tab size for hover
                new_config.settings.editor.tabSize = vim.lsp.util.get_effective_tabstop()
            end
        end,
    --root_dir = root_pattern('tailwind.config.js', 'tailwind.config.ts', 'postcss.config.js', 'postcss.config.ts', 'package.json', 'node_modules', '.git')
    settings = {
        tailwindCSS = {
            classAttributes = { "class", "className", "classList", "ngClass" },
            lint = {
                cssConflict = "warning",
                invalidApply = "error",
                invalidConfigPath = "error",
                invalidScreen = "error",
                invalidTailwindDirective = "error",
                invalidVariant = "error",
                recommendedVariantOrder = "warning"
            },
            validate = true
        }
    }
}


local html_capabilities = vim.lsp.protocol.make_client_capabilities()
html_capabilities.textDocument.completion.completionItem.snippetSupport = true

require'lspconfig'.html.setup {
  capabilities = html_capabilities,
  cmd = { "vscode-html-language-server", "--stdio" },
    filetypes = { "html" },
    init_options = {
      configurationSection = { "html", "css", "javascript" },
      embeddedLanguages = {
        css = true,
        javascript = true
      },
      provideFormatter = true
    }
--    root_dir = function(startpath)
--        return M.search_ancestors(startpath, matcher)
--      end
--    settings = {},
--    single_file_support = true
}
--require'lspconfig'.pylsp.setup{
--    cmd = { "pylsp" },
--    on_attach = on_attach
--    filetypes = { "python" },
--    source_strategies = {"default", "poetry", "system"},
--    settings = {
--        configurationSources = {"flake8"},
--	    formatCommand = {"black"}
--    },
--    root_files = function(fname)
--        local root_files = {
--            'pyproject.toml',
--            'setup.cfg',
--            'requirements.txt',
--            'Pipfile'
--        } 
--        return util.root_pattern(unpack(root_files))(fname) or util.find_git_ancestor(fname) or util.path.dirname(fname)
--        end,
--}
EOF

