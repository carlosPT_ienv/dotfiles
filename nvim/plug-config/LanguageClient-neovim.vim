let g:LanguageClient_loadSettings=1
let g:LanguageClient_useFloatingHover=1
"let g:LanguageClient_diagnosticsDisplay=2
"let g:LanguageClient_autoStart=1
let g:LanguageClient_useVirtualText="Diagnostics"

let g:LanguageClient_serverCommands = {
\   'rust': ['rust-analyzer'],
\   'python': ['$VIRTUAL_ENV/bin/pyls'],
\   'go': ['gopls'],
\}

" 'typescript': ['typescript-language-server']
"rust": {
"clippy_preference": 
""on",
        "all_targets": false,
        "build_on_save": true,
        "wait_to_build": 0
 "   },

