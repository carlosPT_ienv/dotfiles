let g:gruvbox_contrast_dark = 'soft'
let g:gruvbox_contrast_light = 'soft'

let g:gruvbox_guisp_fallback = "bg"
let g:gruvbox_transparent_bg = 1

let g:gruvbox_bold = 1

let g:gruvbox_italicize_comments = 1

let g:gruvbox_invert_indent_guides = 1


colorscheme gruvbox
set background=dark
