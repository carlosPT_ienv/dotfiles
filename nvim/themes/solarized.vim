if has('gui_running')
    syntax enable
    colorscheme solarized
    set background=light
else
    set background=dark
endif
