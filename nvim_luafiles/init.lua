require("carlosp81.plugins-setup")
require("carlosp81.core.options")
require("carlosp81.core.keymaps")
require("carlosp81.core.colorscheme")
require("carlosp81.plugins.comments")
require("carlosp81.plugins.nvim-tree")
require("carlosp81.plugins.lualine")
require("carlosp81.plugins.telescope")
require("carlosp81.plugins.nvim-cmp")
require("carlosp81.plugins.lsp.mason")
require("carlosp81.plugins.lsp.lspsaga")
require("carlosp81.plugins.lsp.lspconfig")
require("carlosp81.plugins.lsp.go_imports")
require("carlosp81.plugins.lsp.null-ls")
require("carlosp81.plugins.autopairs")
-- require("carlosp81.plugins.treesitter")
require("carlosp81.plugins.gitsigns")

vim.cmd([[ 
  augroup vim_enter
    autocmd VimEnter * source ~/.config/nvim/lua/carlosp81/core/keymaps.lua  
  augroup end
]])

vim.cmd([[ 
  augroup vim_enter
    autocmd VimEnter * source ~/.config/nvim/lua/carlosp81/plugins/lsp/lspconfig.lua  
  augroup end
]])

vim.cmd([[ 
  augroup vim_enter
    autocmd BufWritePre *.go lua go_org_imports()  
  augroup end
]])
