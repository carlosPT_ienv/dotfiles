-- import comment plugin safely
local setup, comments = pcall(require, "Comment")
if not setup then
	return
end

-- enable comment
comments.setup()
