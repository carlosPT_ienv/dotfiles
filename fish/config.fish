if status is-interactive
    # Commands to run in interactive sessions can go here
end

# theme
set -g theme_nerd_fonts yes
# set -g theme_powerline_fonts yes
set -g theme_color_scheme nord
# set -g theme_color_scheme zenburn
set -g fish_prompt_pwd_dir_length 1
set -g theme_newline_cursor yes
set -g theme_newline_prompt '$ '
set -g fish_term24bit 1
set -g theme_display_virtualenv yes
set -x VIRTUAL_ENV_DISABLE_PROMPT 1
# Git
set -g theme_display_git_default_branch yes
# set theme_display_git_default_branch yes
set -g theme_display_git yes
# set -g theme_display_git_dirty yes
# set -g theme_display_git_untracked no
# set -g theme_display_git_ahead_verbose yes
set -g theme_display_git_dirty_verbose yes
# set -g theme_display_git_stashed_verbose yes
# set theme_git_worktree_support yes
# set theme_git_default_branches main
# set -g theme_use_abbreviated_branch_name yes

# Git Aliases
alias s="git status"
alias lgp="git lgp"
alias rl="git reflog"
alias cb="git checkout -b"
alias co="git checkout"
alias ba="git branch -a"

#remotes
alias rv="git remote -v"
alias ra="git remote add"
alias rs="git remote show"

#Worktrees
alias gwa="git worktree add"
alias gwr="git worktree remove"
alias gwl="git worktree list"

alias v="nvim.appimage"

#PDFium
export LD_LIBRARY_PATH=/opt/pdfium-linux-x64/lib

# Rust
#export PATH="$HOME/.cargo/bin:$PATH"
set PATH $HOME/.cargo/bin $PATH
set PATH $HOME/.local/bin $PATH

# Dotnet
export DOTNET_ROOT=/usr/share/dotnet
set PATH /home/carlosp81/.local/share/nvm/v16.18.0/bin $PATH
set PATH /home/carlosp81/.dotnet/tools $PATH
set PATH $DOTNET_ROOT $PATH

# Golang
set PATH /usr/local/go/bin $PATH
export GOPATH=(go env GOPATH)
set PATH $GOPATH/bin $PATH  # Custom GoPath

# Pyenv
#export PYENV_ROOT="$HOME/.pyenv"
#export PATH="$PYENV_ROOT/bin:$PATH"
#pyenv init --path | source

# CMD Alias
alias ex="exa --icons --color=always"
alias ls="exa -lh --icons --color=always"
alias lsa="exa -lha --icons --color=always"
alias ed="exa -dh --icons --color=always"
alias et="exa -lTh --icons --color=always --sort=size"
